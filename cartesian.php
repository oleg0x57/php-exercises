<?
/**
 * @param $array
 */
function cartesian($array){
    $tmpCount = 0;
    if(count($array) > 1) {
        foreach ($array as $arr) {
            $tmpCount = (!$tmpCount) ? count($arr) : $tmpCount * count($arr);
        }
        $result = array_fill(0, $tmpCount, '');
        $numToPrint = $tmpCount/count($array[0]);
        foreach ($array as $item) {
            if($item !== $array[0]){
                $numToPrint /= count($item);
            }
            for($i = 0; $i < $tmpCount; $i++){
                if(!current($item)){
                    reset($item);
                }
                $result[$i] .= current($item) . ' ';
                 if(!(($i+1)%($numToPrint))){
                     next($item);
                }
            }
        }
    }
    else{
        $result = $array[0];
    }
    SmartPrint($result);
}
