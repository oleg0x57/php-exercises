<?php
/**
 * @param array $array
 * @return array
 */
function cartesianDirect(array $array){
    $result = [];
    $treshold = $resCount = array_reduce($array, function($count, $arr){return $count *= count($arr);}, 1);
    foreach($array as $key => $arr){
        $treshold /= count($arr);
        for($i = 0; $i < $resCount; $i++){
            if(!current($arr)){
                reset($arr);
            }
            $result[$i][$key] = current($arr);
            if(!(($i + 1) % $treshold)){
                next($arr);
            }
        }
    }
    return $result;
}