<?php
/**
 * @param integer $number
 * @return string
 */
function partitionNumber($number)
{
    $partition[] = $number;
    $partitions[] = $partition;
    while (count($partition) < $number) {
        $first = array_shift($partition);
        if (!empty($partition) && array_sum($partition) / count($partition) > 1) {
            for ($i = count($partition) - 1; $i >= 0; $i--) {
                if ($partition[$i] === 1) {
                    continue;
                }
                $partition[$i]--;
                $partition[] = 1;
                break;
            }
            array_unshift($partition, $first);
        } else {
            $first--;
            $arr = [];
            for ($i = 0; $i < floor($number / $first); $i++) {
                $arr[] = $first;
            }
            if ($number % $first) {
                $arr[] = $number % $first;
            }
            $partition = $arr;
        }
        $partitions[] = $partition;
    }
    return '<pre>' . print_r($partitions, 1) . '</pre>';
}